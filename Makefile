MODULE := gitlab-shell-two-factor-pam

module:
	CGO_ENABLED=1 GOOS=linux go build -buildmode=c-shared -o ${MODULE}.so

clean:
	go clean
	rm -f ${MODULE}.so ${MODULE}.h

.PHONY: module clean
