package main

import (
	client "gitlab.com/gitlab-org/manage/access/pam-gitlab/internal"
	"gitlab.com/gitlab-org/manage/access/pam-gitlab/internal/config"
)

// AuthResult is the result of the authenticate function.
type AuthResult int

const (
	// AuthError is a failure.
	AuthError AuthResult = iota
	// AuthSuccess is a success.
	AuthSuccess
)

func newAPIClient() (*client.Client, error) {
	config, err := config.NewFromDir("/opt/gitlab/embedded/service/gitlab-shell")

	if err != nil {
		return nil, err
	}

	return client.NewClient(config)
}

func authenticate(c *client.Client, gitlabKeyId string, otp string) AuthResult {
	validOTP, err := c.CheckOTP(gitlabKeyId, otp)

	if err != nil {
		return AuthError
	}

	if validOTP {
		return AuthSuccess
	}

	return AuthError
}

func main() {
}
