# Compiling 

```
make module
```

This will generate and SO that is a dynamic object, in other words a dynamic library for *nix

# SSHD config

The `sshd_config.example` file includes an example of the needed configuration to use this module. The sshd config file usually lives at `/etc/ssh/sshd_config`

Example:

```
UsePAM yes
Match User git
  AuthorizedKeysCommand /opt/gitlab/embedded/service/gitlab-shell/bin/gitlab-shell-authorized-keys-check git %u %k
  AuthorizedKeysCommandUser git
  AuthenticationMethods publickey,keyboard-interactive
  PasswordAuthentication no
  PubKeyAuthentication yes
Match all

```


# PAM config

On the previous configuration we enabled pam for login.
And by adding to `/etc/pam.d/sshd`

```
auth required SO_PATH
```

Or to `/etc/pam.conf` (if it is what your system uses)

```
sshd auth required SO_PATH
```

and `SO_PATH` is the compiled module path

# Enable 2FA integration for cli
This feature comes with the `:two_factor_for_cli` feature flag disabled by default.

To enable this feature, ask a GitLab administrator with [Rails console access](https://docs.gitlab.com/ee/administration/feature_flags.html#how-to-enable-and-disable-features-behind-flags) to run the following command:

```
Feature.enable(:two_factor_for_cli, User.find(<user ID>))
```

# Installation (ubuntu amd64)

1. Download the module from https://gitlab.com/gitlab-org/manage/access/pam-gitlab/-/blob/master/gitlab-shell-two-factor-pam.so
1. Make sure the `/etc/ssh/sshd_config` file includes the `UsePAM yes` entry
1. Add at the end of the PAM config (`/etc/pam.d/sshd`) `auth required SO_PATH` where `SO_PATH` is the path where you download and store the module
1. Restart your ssh daemon `/etc/init.d/ssh restart`
1. Enable the feature for 2fa for cli

